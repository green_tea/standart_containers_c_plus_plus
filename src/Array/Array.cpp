//
// Created by root on 28.10.18.
//

#include "Array.h"

template<typename T, unsigned int size>
Array<T, size>::Array() {
    data = new T[size];
}

template<typename T, unsigned int size>
T &Array<T, size>::at(size_t index) {
    if (index >= size) throw std::out_of_range("index out of range");
    return data[index];
}

template<typename T, unsigned int size>
T &Array<T, size>::operator[](size_t index) {
    return data[index];
}
