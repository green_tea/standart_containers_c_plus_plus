//
// Created by root on 28.10.18.
//

#ifndef STANDART_CONTAINERS_C_PLUS_PLUS_ARRAY_H
#define STANDART_CONTAINERS_C_PLUS_PLUS_ARRAY_H

#include<algorithm>
#include <stdexcept>

template <typename T, unsigned size>
class Array {
public:
    Array();

    T& at(size_t index);

    T&operator[](size_t index);

private:
    T* data;

};


#endif //STANDART_CONTAINERS_C_PLUS_PLUS_ARRAY_H

